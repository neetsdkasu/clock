VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "時計"
   ClientHeight    =   1395
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   1650
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1395
   ScaleWidth      =   1650
   StartUpPosition =   2  '画面の中央
   Begin VB.CommandButton Command2 
      Caption         =   "終了"
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "表示"
      Default         =   -1  'True
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim f2 As Form2
Private Sub Command1_Click()
    If f2 Is Nothing Then
        Set f2 = New Form2
    End If
    f2.Show
    SetTopMostWindow f2.hWnd, True
    Visible = False
End Sub

Private Sub Command2_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Set f1 = Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not (f2 Is Nothing) Then
        Unload f2
    End If
    Set f2 = Nothing
End Sub
